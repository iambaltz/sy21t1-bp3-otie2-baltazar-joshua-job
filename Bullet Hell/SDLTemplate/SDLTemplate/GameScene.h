#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Boss.h"
#include "Power.h"
#include "text.h"
#include <vector>

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

	std::vector<Bullet*> phantomBullet;
private:
	Player* player;
	Enemy* enemy;
	Power* powerUp;

	// Enemy spawning logic
	float spawnTime;
	float currentSpawnTimer;

	// Power up spawning logic

	float powerUpSpawnTime;
	float currentPowerUpSpawnTime;

	std::vector<Enemy*> spawnedEnemies;
	std::vector<Power*> spawnedPower;
	

	void checkSpawn();
	void collisionLogic();
	void checkPowerSpawn();
	void spawn();
	void spawnPowerUp();
	void despawnEnemy(Enemy* enemy);
	void despawnPower(Power* power);
	void handlePhantomBullets();

	void bossScene();

	int points;
	bool isBoss;
};

