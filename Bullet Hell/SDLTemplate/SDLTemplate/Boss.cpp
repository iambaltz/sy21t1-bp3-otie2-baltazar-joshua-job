#include "Boss.h"
#include "Scene.h"

Boss::Boss()
{
}

Boss::~Boss()
{

}

void Boss::start()
{
	texture = loadTexture("gfx/boss.png");

	// Init to avoid garbage values
	directionX = 1;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2;
	reloadTime = 45;
	mHp = 1500;
	angleT = 0;
	currentReloadTime = 0;
	directionChangeTime = (rand() % 50) + 180;
	currentDirectionChangeTime = 0;
	patternChangeTime = 7;
	currentPatternChangeTime = 5;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 64;

}

void Boss::update()
{
	// Move

	y += directionY;

	if (x > 720) {
		x = 0;
	}

	if (x < 0) {

		x = 720;
	}

	if (y >= 360 /2) {

		x += directionX * speed;
		directionY = 0;

		// Basic AI, switch directions every X seconds
		if (currentDirectionChangeTime > 0) {

			currentDirectionChangeTime--;

		}

		std::cout << mHp << std::endl;

		if (currentDirectionChangeTime == 0) {

			// Flip Direction
			directionX = -directionX;
			currentDirectionChangeTime = directionChangeTime;

		}

		// Decrement the enemy's reload timer
		if (currentReloadTime > 0) {

			currentReloadTime--;

		}

		// Fire if timer is ready
		if (currentReloadTime == 0) {

			float dx = -1;
			float dy = 0;

			calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);

			SoundManager::playSound(sound);

			if (mHp > 500) {
				
				if (currentPatternChangeTime > 0) {
					for (int i = 0; i < 4; i++) {
						SoundManager::playSound(sound);
						Bullet* bullet = new Bullet(x - 2 + ((width * i) / 2), y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE, loadTexture("gfx/bossBullet.png"));
						bullets.push_back(bullet);
						getScene()->addGameObject(bullet);
					}
					std::cout << currentPatternChangeTime << std::endl;
					currentPatternChangeTime--;
				}

				if (currentPatternChangeTime == 0) {
					
					for (int i = 0; i < 10; i++) {
						SoundManager::playSound(sound);
						Bullet* bullet = new Bullet(x - 2 + ((width * i) / 2), y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE, loadTexture("gfx/bossBullet.png"));
						bullets.push_back(bullet);
						getScene()->addGameObject(bullet);
					}

					currentPatternChangeTime = patternChangeTime;
				}

			}
			
			if (mHp <= 700 && mHp >= 500) {
				bulletRing();
			}

			
			if (mHp <= 500) {
				
				bulletSpiral(0.025f, 3);
			}

			// After firing, reset our reload timer
			currentReloadTime = reloadTime;

		}
	}

	// Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++) {

		if (bullets[i]->getPositionY() > SCREEN_HEIGHT || bullets[i]->getPositionX() > SCREEN_WIDTH) {

			// Cache the variable to we can delete it later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			break;
		}
	}
}

void Boss::draw()
{
	blit(texture, x, y);
}

int Boss::getHp()
{
	return mHp;
}

void Boss::setHp(int newHp)
{
	mHp = newHp;
}

void Boss::bulletRing()
{
	
	float angle = 0;

		for (int i = 0; i < 10; i++) {

			Bullet* bullet = new Bullet(x + 30 * cos(angle) + width / 2, y + 30 * sin(angle) + height / 2, 30 * cos(angle), 30 * sin(angle), 0.25f, Side::ENEMY_SIDE, loadTexture("gfx/bulletRing.png"));
			bullets.push_back(bullet);

			getScene()->addGameObject(bullet);

			currentReloadTime = reloadTime;

			angle += 360 / 3;
		}
	
	angle = 0;
}

void Boss::bulletSpiral(float rotation, int frequency) // Say goodbye to the player ;)
{
	for (int i = 0; i < 3; i++) {
		reloadTime = 2;
		// Where the bullet spawns 
		// x pos formula x + radius * cos(angle)
		// y pos forumla is y + radius * cos(angle)

		// Where the bullet goes
		// X pos formula is radius * cos(angle)
		// Y pos forumla is radius * sin(angle)

		// Angle increases over time, with this, the spiral is formed?


		Bullet* bullet = new Bullet(x + 30 * cos(angleT) + width / 2 , y + 30 * sin(angleT) + height / 2, 180 * cos(angleT), 180 * sin(angleT), 0.025, Side::ENEMY_SIDE, loadTexture("gfx/bulletSpiral.png"));
		bullets.push_back(bullet);

		getScene()->addGameObject(bullet);

		currentReloadTime = reloadTime;
		angleT += rotation * (360 / frequency); // 360 / frequency
	}
}
