#pragma once
#include "Enemy.h"
#include "Bullet.h"


class Boss :
    public Enemy
{
public:
    Boss();
    ~Boss() override;

    void start() override;
    void update() override;
    void draw() override;

    int getHp();
    void setHp(int newHp);
    void bulletRing();
    void bulletSpiral(float rotation, int frequency);

private:

    int mHp;
    int patternChangeTime;
    int currentPatternChangeTime;
    float angleT;

};

