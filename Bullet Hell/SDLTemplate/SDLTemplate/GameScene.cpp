#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;

}

GameScene::~GameScene()
{
	delete player;
	delete enemy;
	delete powerUp;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	isBoss = false;
	currentSpawnTimer = 300;
	spawnTime = 300; // Spawn time of 5 seconds

	currentPowerUpSpawnTime = 500;
	powerUpSpawnTime = rand() % 1000 + 500;

	for (int i = 0; i < 3; i++) {

		Enemy* enemy = new Enemy();
		this->addGameObject(enemy);
		enemy->setPlayerTarget(player);

		enemy->setPosition(360 + (rand() % 100), -100); //X = 720 Y = 1280
		spawnedEnemies.push_back(enemy);
	}
}

void GameScene::draw()
{
	Scene::draw();
	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false) {

		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();

	checkSpawn();
	checkPowerSpawn();
	collisionLogic();
	handlePhantomBullets();
	

	for (int i = 0; i < spawnedPower.size(); i++) {

		if (spawnedPower[i]->getPositionY() > SCREEN_HEIGHT) {

			// Cache the variable to we can delete it later
			Power* powerToErase = spawnedPower[i];
			spawnedPower.erase(spawnedPower.begin() + i);
			delete powerToErase;

			break;
		}

	}

	if (points % 15 == 0 && points != 0 && isBoss == false) {
		isBoss = true;
		bossScene();
	}

	for (int i = 0; i < spawnedEnemies.size(); i++) {

		if (spawnedEnemies[i]->getPositionY() > SCREEN_HEIGHT) {

			Enemy* enemyToErase = spawnedEnemies[i];
			spawnedEnemies.erase(spawnedEnemies.begin() + i);
			delete enemyToErase;

			break;
		}


	}
}

void GameScene::checkSpawn()
{
	if (currentSpawnTimer > 0) {

		currentSpawnTimer--;
	}

	if (currentSpawnTimer <= 0) {

		for (int i = 0; i < 6; i++) {

			spawn();

		}

		currentSpawnTimer = spawnTime;

	}

}

void GameScene::collisionLogic()
{
	// Check for collision
	for (int i = 0; i < objects.size(); i++) {

		// Cast to bullet
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);
		Power* power = dynamic_cast<Power*>(objects[i]);

		if (power != NULL) {

			int collision = checkCollision(

				player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
				power->getPositionX(), power->getPositionY(), power->getWidth(), power->getHeight()

			);

			if (collision == 1) {

				if (player->getIsPowered() == true) { // If powered, extend power time

					player->setCurrentPowerTime(player->getPowerTime());

				}

				else { // If not powered then make player powered

					player->doPowered();

				}

				despawnPower(power);
				//player->setBulletCount(player->getBulletCount() + 1); This is for the powerup stack feature
				break;
			}


		}

		// Check if cast was successful (i.e objects[i] is a Bullet)
		if (bullet != NULL) {

			// If Bullet is from enemy side, check against the player
			if (bullet->getSide() == Side::ENEMY_SIDE) {

				int collision = checkCollision(

					player->getPositionX(), player->getPositionY() , player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()

				);

				if (collision == 1) {

					player->doDeath();
					break;
				}
			}

			// If bullet is from player side, check against the enemy
			else if (bullet->getSide() == Side::PLAYER_SIDE) {

				for (int i = 0; i < spawnedEnemies.size(); i++) {

					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(

						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()

					);

					if (collision == 1) {

						Boss* boss = dynamic_cast<Boss*>(spawnedEnemies[i]);
						if (boss == NULL) {
							despawnEnemy(currentEnemy);
							points++;
						}
						
						else {
							boss->setHp(boss->getHp() - 1);

							if (boss->getHp() <= 0) {

								despawnEnemy(boss);
								points++;
								isBoss = false;
								

							}
						}
						// Put break to stop the loop
						break;

					}
				}
			}

		}

	}


}

void GameScene::checkPowerSpawn()
{
	if (currentPowerUpSpawnTime > 0) {

		currentPowerUpSpawnTime--;

	}

	if (currentPowerUpSpawnTime <= 0) {

		spawnPowerUp();
		currentPowerUpSpawnTime = powerUpSpawnTime;

	}
}

void GameScene::spawn()
{
	if (isBoss == false) {
		Enemy* enemy = new Enemy();
		this->addGameObject(enemy);
		enemy->setPlayerTarget(player);

		enemy->setPosition(rand() % 720, -100);
		spawnedEnemies.push_back(enemy);
	}

}

void GameScene::spawnPowerUp()
{
	Power* power = new Power();
	this->addGameObject(power);

	power->setPosition(rand() % 720, -100);
	spawnedPower.push_back(power);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++) {

		if (enemy == spawnedEnemies[i]) {

			index = i;
			break;

		}
	}

	if (index != -1) {

		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;

	}
}

void GameScene::despawnPower(Power* power)
{
	int index = -1;
	for (int i = 0; i < spawnedPower.size(); i++) {

		if (power == spawnedPower[i]) {

			index = i;
			break;

		}

	}

	if (index != -1) {

		spawnedPower.erase(spawnedPower.begin() + index);
		delete power;
	}
}

void GameScene::handlePhantomBullets()
{
	for (int i = 0; i < phantomBullet.size(); i++) {

		Bullet* bulletToErase = phantomBullet[i];

		if (bulletToErase->getPositionY() > SCREEN_HEIGHT || bulletToErase->getPositionY() < 0 || bulletToErase->getPositionX() > SCREEN_WIDTH || bulletToErase->getPositionX() < 0) {

			phantomBullet.erase(phantomBullet.begin() + i);
			delete bulletToErase;


			break;
		}

	}
}

void GameScene::bossScene()
{
		Boss* boss = new Boss();
		this->addGameObject(boss);
		boss->setPlayerTarget(player);

		boss->setPosition(SCREEN_WIDTH / 2, -100);
		spawnedEnemies.push_back(boss);
		//points++;

}


