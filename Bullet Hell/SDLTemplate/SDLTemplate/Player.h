#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>

class Player :
    public GameObject
{
public:
	~Player();
	void start();
	void update();
	void draw();

	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	int getBulletCount();
	int getPowerTime();
	
	bool getIsPowered();
	bool getIsAlive();
	

	void doDeath();
	void doPowered();
	void setBulletCount(int bullets);
	void setCurrentPowerTime(int time);
	

private:
	SDL_Texture* texture;
	Mix_Chunk* sound;

	int x;
	int y;
	int width;
	int height;
	int speed;
	int bulletCount;
	int powerTime;
	int currentPowerTime;
	
	float reloadTime;
	float currentReloadTime;

	std::vector<Bullet*> bullets;
	bool isAlive;
	bool isPowered;
	
};

