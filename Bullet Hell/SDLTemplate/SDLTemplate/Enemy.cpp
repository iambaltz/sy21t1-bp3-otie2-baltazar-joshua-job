#include "Enemy.h"
#include "Scene.h"
#include "GameScene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{

	for (int i = 0; i < bullets.size(); i++) {

		((GameScene*)this->getScene())->phantomBullet.push_back(bullets[i]);
	
		
	}

	bullets.clear();
}

void Enemy::start()
{
	// Load Texture
	texture = loadTexture("gfx/enemy.png");

	// Init to avoid garbage values
	directionX = 1;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2;
	reloadTime = 60;
	currentReloadTime = 0;
	directionChangeTime = (rand() % 100) + 180;
	currentDirectionChangeTime = 0;

	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 64;
}

void Enemy::update()
{
	// Move
	x += directionX * speed;
	y += directionY * speed;

	if (x > 720) {
		x = 0;
	}

	if (x < 0) {

		x = 720;
	}

	// Basic AI, switch directions every X seconds
	if (currentDirectionChangeTime > 0) {

		currentDirectionChangeTime--;

	}

	if (currentDirectionChangeTime == 0) {

		// Flip Direction
		directionX = -directionX;
		currentDirectionChangeTime = directionChangeTime;

	}

	// Decrement the enemy's reload timer
	if (currentReloadTime > 0) {

		currentReloadTime--;

	}

	// Fire if timer is ready
	if (currentReloadTime == 0) {

		float dx = -1;
		float dy = 0;

		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x + width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		bullets.push_back(bullet);

		getScene()->addGameObject(bullet);
		bulletRing();
		// After firing, reset our reload timer
		currentReloadTime = reloadTime;

	}

	// Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++) {

		if (bullets[i]->getPositionY() > SCREEN_HEIGHT || bullets[i]->getPositionX() > SCREEN_WIDTH) {

			// Cache the variable to we can delete it later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			break;
		}
	}

}


void Enemy::draw()
{
	blit(texture, x, y);
}

void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Enemy::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Enemy::getPositionX()
{
	return x;
}

int Enemy::getPositionY()
{
	return y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}

void Enemy::bulletRing()
{
	float angle = 0;

	for (int i = 0; i < 10; i++) {

		Bullet* bullet = new Bullet(x + 20 * cos(angle), y + 20 * sin(angle), 30 * cos(angle), 30 * sin(angle), 0.25f, Side::ENEMY_SIDE, loadTexture("gfx/bulletRing.png"));
		bullets.push_back(bullet);

		getScene()->addGameObject(bullet);

		currentReloadTime = reloadTime;

		angle += 360 / 3;
	}

	angle = 0;
}
