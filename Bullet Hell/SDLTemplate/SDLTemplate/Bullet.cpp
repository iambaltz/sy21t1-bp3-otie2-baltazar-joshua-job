#include "Bullet.h"

Bullet::Bullet(float positionX, float positionY, float directionX, float directionY, float speed, Side side)
{

	this->x = positionX;
	this->y = positionY;
	this->directionX = directionX;
	this->directionY = directionY;
	this->speed = speed;
	this->side = side;

	if (side == Side::PLAYER_SIDE) {
		texture = loadTexture("gfx/playerBullet.png");
	}
	else if (side == Side::ENEMY_SIDE) {
		texture = loadTexture("gfx/alienBullet.png");
	}

}

Bullet::Bullet(float positionX, float positionY, float directionX, float directionY, float speed, Side side, SDL_Texture* newTexture)
{
	this->x = positionX;
	this->y = positionY;
	this->directionX = directionX;
	this->directionY = directionY;
	this->speed = speed;
	this->side = side;
	this->texture = newTexture;
}

void Bullet::start()
{
	width = 0;
	height = 0;
	

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

}

void Bullet::update()
{
	x += directionX * speed;
	y += directionY * speed;
}

void Bullet::draw()
{
	blit(texture, x, y);
}

float Bullet::getPositionX()
{
	return x;
}

float Bullet::getPositionY()
{
	return y;
}

float Bullet::getWidth()
{
	return width;
}

float Bullet::getHeight()
{
	return height;
}

Side Bullet::getSide()
{
	return side;
}
