#include "Player.h"
#include "Scene.h"

Player::~Player()
{

	for (int i = 0; i < bullets.size(); i++) {

		delete bullets[i];

	}

	bullets.clear();
}

void Player::start()
{
	// Load Texture
	texture = loadTexture("gfx/marisa.png");

	// Init to avoid garbage values
	x = 360;
	y = 1000;
	width = 0;
	height = 0;
	speed = 6;
	reloadTime = 10;
	currentReloadTime = 0;
	powerTime = 100;
	currentPowerTime = 100;
	bulletCount = 3; // This is for the power up
	isAlive = true;
	isPowered = false;
	
	// Query the texture to set our width and height
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Player::update()
{
	texture = loadTexture("gfx/marisa.png");

	// Memory manage our bullets. When they go off screen, delete them
	for (int i = 0; i < bullets.size(); i++) {

		if (bullets[i]->getPositionY() < 0) {

			// Cache the variable to we can delete it later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			break;
		}
	}

	if (!isAlive) return;

	if (app.keyboard[SDL_SCANCODE_W]) {

		y -= speed;
	}

	if (app.keyboard[SDL_SCANCODE_S]) {

		y += speed;
	}

	if (app.keyboard[SDL_SCANCODE_A]) {

		x -= speed;
		texture = loadTexture("gfx/marisaLeft.png");
	}

	if (app.keyboard[SDL_SCANCODE_D]) {

		x += speed;
		texture = loadTexture("gfx/marisaRight.png");
	}

	
	//SDL_GetMouseState(&x, &y);

	// Pos Check

	if (x > 720) {
		x = 0;
	}

	if (x < 0) {

		x = 720;
	}

	// Decrement the player's reload timer
	if (currentReloadTime > 0) {

		currentReloadTime--;

	}


	if (app.keyboard[SDL_SCANCODE_F] && currentReloadTime == 0) {

		if (isPowered == true) {

			if (currentPowerTime > 0) {

				for (int i =0; i < bulletCount; i++){
					SoundManager::playSound(sound);
					Bullet* bullet = new Bullet(x - 2 + ((width * i) / 2), y - 2 + height / 2, 0, -1, 10, Side::PLAYER_SIDE);
					bullets.push_back(bullet);
					getScene()->addGameObject(bullet);
				}
				/*
				SoundManager::playSound(sound);
				Bullet* bullet = new Bullet(x - 2 + width / 2, y - 2 + height / 2, 0, -1, 10, Side::PLAYER_SIDE);
				Bullet* bullet2 = new Bullet(x - 30 + width / 2, y - 2 + height / 2, 0, -1, 10, Side::PLAYER_SIDE);
				Bullet* bullet3 = new Bullet(x - (-30) + width / 2, y - 2 + height / 2, 0, -1, 10, Side::PLAYER_SIDE);
				bullets.push_back(bullet);
				bullets.push_back(bullet2);
				bullets.push_back(bullet3);
				getScene()->addGameObject(bullet);
				getScene()->addGameObject(bullet2);
				getScene()->addGameObject(bullet3);
				*/
				
				currentPowerTime--;
			}

			if (currentPowerTime <= 0) {

				isPowered = false;
				currentPowerTime = powerTime;
			}

		}

		else {
			SoundManager::playSound(sound);
			Bullet* bullet = new Bullet(x - 2 + width / 2, y - 2 + height / 2, 0, -1, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);
		}

		// After firing, reset our reload timer
		currentReloadTime = reloadTime;

	}
}

void Player::draw()
{
	if (!isAlive) return;
	blit(texture, x, y);
	
}

int Player::getPositionX()
{
	return x;
}

int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

int Player::getBulletCount()
{
	return bulletCount;
}

int Player::getPowerTime()
{
	return powerTime;
}


bool Player::getIsPowered()
{
	return isPowered;
}

bool Player::getIsAlive()
{
	return isAlive;
}

void Player::doDeath()
{
	isAlive = false;
}

void Player::doPowered()
{
	isPowered = true;
}

void Player::setBulletCount(int bullets)
{
	bulletCount = bullets;
}

void Player::setCurrentPowerTime(int time)
{
	currentPowerTime = time;
}
