#pragma once
#include "GameObject.h"
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include <vector>
#include "util.h"

class Power :
    public GameObject
{
public:
    Power();
    ~Power();
    void start();
    void update();
    void draw();

    void setPosition(int xPos, int yPos);
    int getPositionX();
    int getPositionY();
    int getWidth();
    int getHeight();

private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    int x;
    int y;
    int speed;
    int width;
    int height;
    float directionX;
    float directionY;
};

