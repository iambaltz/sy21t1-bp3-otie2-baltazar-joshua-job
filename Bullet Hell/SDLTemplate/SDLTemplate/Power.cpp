#include "Power.h"

Power::Power()
{
}

Power::~Power()
{
}

void Power::start()
{
	// Load Texture
	texture = loadTexture("gfx/points.png");
	directionX = 1;
	directionY = 1;
	speed = 2;


	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 64;
}

void Power::update()
{
	y += directionY * 2;
}

void Power::draw()
{
	blit(texture, x, y);
}

void Power::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Power::getPositionX()
{
	return x;
}

int Power::getPositionY()
{
	return y;
}

int Power::getWidth()
{
	return width;
}

int Power::getHeight()
{
	return height;
}
