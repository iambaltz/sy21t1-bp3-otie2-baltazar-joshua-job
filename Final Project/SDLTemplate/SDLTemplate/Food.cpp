#include "Food.h"

void Food::start()
{
	texture = loadTexture("gfx/food.png");
	x = 0;
	y = 0;

	width = 0;
	height = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Food::update()
{

}

void Food::draw()
{
	blit(texture,x, y);
}

void Food::setPosition(int _x, int _y)
{
	this->x = _x;
	this->y = _y;
}

int Food::getX()
{
	return x;
}

int Food::getY()
{
	return y;
}

int Food::getWidth()
{
	return width;
}

int Food::getHeight()
{
	return height;
}
