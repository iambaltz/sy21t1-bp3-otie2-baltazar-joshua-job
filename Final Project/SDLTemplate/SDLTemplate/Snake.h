#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "util.h"
#include <vector>

struct Segment {
    int x;
    int y;
    Segment(int _x, int _y) {
        x = _x;
        y = _y;
    }
};

class Snake :
    public GameObject
{
public:

    ~Snake();
    
    void start();
    void update();
    void draw();

    void addSegment(int x, int y);
    void move();
    void checkScreen();
    void selfCollision();

    int getWidth();
    int getHeight();
    bool getIsAlive();

    std::vector<Segment*> const& getBody() const;
   
private:

    std::vector<Segment*> body;

    SDL_Texture* texture;

    int x;
    int y;

    int dx;
    int dy;

    int width;
    int height;

    bool isAlive;

    float speed;
    
    
};

