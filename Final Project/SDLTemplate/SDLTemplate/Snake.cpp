#include "Snake.h"

Snake::~Snake()
{
	for (int i = 0; i < body.size(); i++) {

		delete body[i];

	}

	body.clear();
}

void Snake::start()
{
	texture = loadTexture("gfx/player.png");

	x = CELL_WIDTH / 2;
	y = CELL_HEIGHT / 2;

	dx = 0;
	dy = 0;

	width = 0;
	height = 0;

	isAlive = true;

	speed = 1;

	addSegment(x, y);

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Snake::update()
{
	move();
	checkScreen();
	selfCollision();
}

void Snake::draw()
{
	//if (!isAlive) return; // Cancel the render if player dies

	for (int i = 0; i < body.size(); i++) {

		blit(texture, body[i]->x, body[i]->y);

	}
}

void Snake::addSegment(int x, int y)
{
	Segment* seg = new Segment(x, y);
	body.push_back(seg);
}

void Snake::move()
{
	if (app.keyboard[SDL_SCANCODE_W] && dy != 1) {
		dx = 0;
		dy = -speed;
	}

	if (app.keyboard[SDL_SCANCODE_A] && dx != 1) {
		dx = -speed;
		dy = 0;
	}

	if (app.keyboard[SDL_SCANCODE_S] && dy != -1) {
		dx = 0;
		dy = speed;
	}

	if (app.keyboard[SDL_SCANCODE_D] && dx != -1) {
		dx = speed;
		dy = 0;
	}

	for (int i = body.size() - 1; i > 0; i--) { 

		body[i]->x = body[i - 1]->x;
		body[i]->y = body[i - 1]->y;

	}

	Segment* snakeHead = *(body.begin());

	snakeHead->x += dx;
	snakeHead->y += dy;
}

void Snake::checkScreen()
{
	for (int i = 0; i < body.size(); i++) {
		if (body[i]->x > CELL_WIDTH) {

			body[i]->x = 0;

		}
		if (body[i]->x < 0) {

			body[i]->x = CELL_WIDTH - 1;

		}

		if (body[i]->y > CELL_HEIGHT) {

			body[i]->y = 0;

		}

		if (body[i]->y < 0) {

			body[i]->y = CELL_HEIGHT - 1;

		}
	}
}

void Snake::selfCollision()
{
	for (int i = 1; i < body.size(); i++) {

		if (body[0]->x == body[i]->x && body[0]->y == body[i]->y) {
			isAlive = false;
			break;
		}
	}
}

int Snake::getWidth()
{
	return width;
}

int Snake::getHeight()
{
	return height;
}

bool Snake::getIsAlive()
{
	return isAlive;
}

std::vector<Segment*> const& Snake::getBody() const
{
	// TODO: insert return statement here
	return body;
}
