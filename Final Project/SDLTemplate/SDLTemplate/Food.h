#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <vector>

class Food :
    public GameObject
{
public:
    void start();
    void update();
    void draw();

    void setPosition(int _x, int _y);

    int getX();
    int getY();

    int getWidth();
    int getHeight();

private:

    SDL_Texture* texture;

    int x;
    int y;

    int width;
    int height;


};

