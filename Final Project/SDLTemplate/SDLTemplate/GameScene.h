#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Snake.h"
#include "Food.h"
#include "text.h"

class GameScene : public Scene
{
public:

	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

	void spawnFood();
	void despawnFood(Food* food);
	void collision();
	
	
private:

	Snake* snake;

	int points;
	int hiScore;
	
	std::vector<Food*> spawnedFood;
};

