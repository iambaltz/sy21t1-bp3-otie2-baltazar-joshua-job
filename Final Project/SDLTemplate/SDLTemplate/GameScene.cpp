#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	snake = new Snake();
	this->addGameObject(snake);
	points = 0;
}

GameScene::~GameScene()
{
	delete snake;
}

void GameScene::start()
{
	Scene::start();
	spawnFood();
	initFonts();
	// Initialize any scene logic here
}

void GameScene::draw()
{
	Scene::draw();
	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);
	drawText(510, 20, 255, 255, 255, TEXT_CENTER, "HISCORE: %03d", hiScore);
}

void GameScene::update()
{
	Scene::update();

	if (spawnedFood.size() == 0 && spawnedFood.size() != 1) {
		spawnFood();
	}
	collision();

	if (snake->getIsAlive() != true) {
		delete snake;
		snake = new Snake();
		this->addGameObject(snake);

		if (points > hiScore) {
			hiScore = points;
		}
		points = 0;
	}

}


void GameScene::spawnFood()
{
	Food* food = new Food();
	this->addGameObject(food);

	food->setPosition(rand() % CELL_WIDTH, rand() % CELL_HEIGHT);

	spawnedFood.push_back(food);
}


void GameScene::despawnFood(Food* food)
{
    int index = -1;
    for (int i = 0; i < spawnedFood.size(); i++) {

        if (food == spawnedFood[i]) {

            index = i;
            break;

        }

    }

    if (index != -1) {

        spawnedFood.erase(spawnedFood.begin() + index);
        delete food;
    }
}

void GameScene::collision()
{
	Segment* head = snake->getBody()[0];
	std::vector<Segment*> snakeBody = snake->getBody();
	
	if (head->x  == spawnedFood[0]->getX()  && head->y  == spawnedFood[0]->getY()) {

		despawnFood(spawnedFood[0]);
		std::cout << "hit";
		snake->addSegment(snakeBody[snakeBody.size() - 1]->x, snakeBody[snakeBody.size() - 1]->y); //Adds a part to the snake
		points++;

	}

}



  



